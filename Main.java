package hr.turic.faculty;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;

public class Main {

    private static final long DAY = 60 * 60 * 24 * 1000;
    private static final long HOUR = 60 * 60 * 1000;
    private static final String SPLITER = ",";
    private static final String HEADER = "Naziv Stola,broj ljudi" +
            ",vrijeme dolaska(unix),vrijeme dolaska(HH:mm),vrijeme cekanja(sec),vrijeme cekanja(mm:ss)" +
            ",vrijeme narudzbe(unix),vrijeme narudzbe(HH:mm),vrijeme serviranja(sec), vrijeme serviranja(mm:ss)" +
            ",vrijeme serviranje(unix),vrijeme serviranje(HH:mm),vrijeme sjedenja(sec), vrijeme sjedenja(HH:mm:ss)" +
            ",vrijeme odlaska(unix),vrijeme odlaska(HH:mm),vrijeme same usluge(sec),vrijeme same usluge(mm:ss)\n";

    private static final String FOLDER = "test/";

    private static LinkedList<String> data = new LinkedList<>();
    private static LinkedList<Long> times = new LinkedList<>();

    public static void main(String[] args) {
        // write your code here

        File file = new File(FOLDER + args[0]);
        File output = new File(FOLDER + "output.csv");

        BufferedReader reader = null;
        FileWriter writer;
        String put = "";
        try {
            reader = new BufferedReader(new FileReader(file));
            writer = new FileWriter(output);

            String s = reader.readLine();
            writer.write(HEADER);
            while (s != null) {
                put = parseInput(s);
                s = reader.readLine();
            }

            for (String string : data) {
                writer.write(string + System.lineSeparator());
            }

            writer.flush();

            writer.close();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }

        createFiles();
        counter();
        counterTaken();
        arrivalHourCount();
    }

    private static void counter() {
        int[] result;
        for (int i = 0; i < 5; i++) {
            File file = new File(FOLDER + "output_" + termins[i] + ".csv");
            result = new int[6];

            try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

                reader.readLine();
                String line = reader.readLine();

                while (line != null) {
                    String[] split = line.split(",");

                    String s = split[16];

                    int mod = Integer.parseInt(s);

                    result[mod / 60]++;

                    line = reader.readLine();
                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            File output = new File(FOLDER + "count.csv");
            if (i == 0) {
                try {
                    if (output.exists()) {
                        output.delete();
                        output.createNewFile();
                    } else {
                        output.createNewFile();
                    }
                } catch (Exception e) {
                }
            }

            try (FileWriter fileWriter = new FileWriter(output, true)) {

                String replace = Arrays.toString(result).replace("[", "").replace("]", "").replace(" ", "").trim();

                fileWriter.write(replace + "\n");
                fileWriter.flush();

                System.out.println(replace);

            } catch (Exception e) {

            }
        }
    }

    private static void counterTaken() {
        int[] result;
        for (int i = 0; i < 5; i++) {
            File file = new File(FOLDER + "output_" + termins[i] + ".csv");
            result = new int[8];

            try (BufferedReader read = new BufferedReader(new FileReader(file))) {

                read.readLine();
                String line = read.readLine();

                while (line != null) {
                    String[] split = line.split(",");

                    String s = split[12];

                    int mod = Integer.parseInt(s);

                    result[mod / (60 * 15)]++;

                    line = read.readLine();
                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            File taken = new File(FOLDER + "count_taken.csv");
            if (i == 0) {
                try {
                    if (taken.exists()) {
                        taken.delete();
                        taken.createNewFile();
                    } else {
                        taken.createNewFile();
                    }
                } catch (Exception e) {
                }
            }

            try (FileWriter fileWriter = new FileWriter(taken, true)) {

                String replace = Arrays.toString(result).replace("[", "").replace("]", "").replace(" ", "").trim();

                fileWriter.write(replace + "\n");
                fileWriter.flush();

                System.out.println(replace);

            } catch (Exception e) {

            }
        }
    }

    private static String[] termins = {"08-11", "11-14", "14-17", "17-20", "20-23", "00_00"};
    private static int[] hours = {7, 10, 13, 16, 19, 23};

    private static void createFiles() {
        File output;
        FileWriter writer = null;

        int i = 0;

        try {
            for (int j = 0; j < data.size(); j++) {
                if ((times.get(j) % DAY) > (HOUR * hours[i])) {
                    if (writer != null) {
                        writer.close();
                    }

                    output = getOutputFile(i);
                    writer = new FileWriter(output);
                    writer.write(HEADER);
                    i++;
                }

                writer.write(data.get(j) + System.lineSeparator());
            }

            writer.flush();

            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static File getOutputFile(int index) throws IOException {
        File file = new File(FOLDER + "output_" + termins[index] + ".csv");

        if (!file.exists()) {
            file.createNewFile();
        }

        return file;
    }

    static private SimpleDateFormat formatmmss = new SimpleDateFormat("mm:ss");
    static private SimpleDateFormat formatHHmm = new SimpleDateFormat("HH:mm");
    static private SimpleDateFormat formatHHmmss = new SimpleDateFormat("HH:mm:ss");

    private static String parseInput(String s) {
        String[] split = s.split(",");

        if (split.length < 6) return "";

        String name = split[0]; //name
        String count = split[1]; //broj ljudi
        String start = split[2]; //vrijeme dolaska
        String order = split[3]; //vrijeme narudzbe
        String serve = split[4]; //vrijeme serviranja
        String leave = split[5]; //vrijeme odlaska

        long startTime = Long.parseLong(start);
        long orderTime = Long.parseLong(order);
        long serveTime = Long.parseLong(serve);
        long leaveTime = Long.parseLong(leave);

        Date startDate = new Date(startTime);
        Date orderDate = new Date(orderTime);
        Date serveDate = new Date(serveTime);
        Date leaveDate = new Date(leaveTime);

        if ((leaveTime - startTime) > 30 * 1000 && (leaveTime - startTime) < 10 * 60 * 1000) return "";

        long sittingTime = leaveTime - serveTime;
        long servingTime = serveTime - orderTime;
        long waitingTime = orderTime - startTime;


        StringBuilder builder = new StringBuilder();

        builder.append(name).append(SPLITER);
        builder.append(count).append(SPLITER);

        builder.append(startTime / 1000).append(SPLITER);
        builder.append(formatHHmm.format(startDate)).append(SPLITER);
        builder.append(String.valueOf(waitingTime / 1000)).append(SPLITER);
        builder.append(formatmmss.format(new Date(waitingTime))).append(SPLITER);

        builder.append(orderTime / 1000).append(SPLITER);
        builder.append(formatHHmm.format(orderDate)).append(SPLITER);
        builder.append(String.valueOf(servingTime / 1000)).append(SPLITER);
        builder.append(formatmmss.format(new Date(servingTime))).append(SPLITER);

        builder.append(serveTime / 1000).append(SPLITER);
        builder.append(formatHHmm.format(serveDate)).append(SPLITER);
        builder.append(String.valueOf(sittingTime / 1000)).append(SPLITER);
        builder.append(formatHHmmss.format(new Date(new Date(sittingTime).getTime() - 3600000))).append(SPLITER);

        builder.append(leaveTime / 1000).append(SPLITER);
        builder.append(formatHHmm.format(leaveDate)).append(SPLITER);

        builder.append((serveTime - startTime) / 1000).append(SPLITER);
        builder.append(formatmmss.format(new Date(serveTime - startTime)));

        apppend(startTime, builder.toString());

        return builder.toString();
    }

    private static int[] hourArrivals = new int[23];

    private static void arrivalHourCount() {
        File file = new File(FOLDER + "/output.csv");

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

            String line = reader.readLine();

            while (line != null) {

                String[] split = line.split(",");
                String[] hour = split[3].split(":");

                try {
                    int i = Integer.parseInt(hour[0]);

                    hourArrivals[i]++;

                    line = reader.readLine();
                } catch (NumberFormatException e) {
                    line = reader.readLine();
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < hourArrivals.length; i++) {
            System.out.print(String.format("%2d ", i));
        }
        System.out.println();
        for (int i = 0; i < hourArrivals.length; i++) {
            System.out.print(String.format("%2d ", hourArrivals[i]));
        }
    }
    private static void apppend(long time, String line) {
        for (int i = 0; i < data.size(); i++) {
            if ((times.get(i) % DAY) > time % DAY) {
                times.add(i, time);
                data.add(i, line);

                return;
            }
        }

        times.add(time);
        data.add(line);
    }
}
